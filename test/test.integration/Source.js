function StringCalculator() {
    this.add = function (numbers) {
        var delimeter = ',';

        if (numbers.indexOf('//') == 0) {
            delimeter = numbers.substr(2, 1);
            numbers = numbers.substr(4);
        }

        if (!numbers) {
            return 0;
        }

        var sum = 0;
        var errors = [];

        _.chain(numbers.split(delimeter))                   // separate by delimeter
            .map(function (n) { return n.split('\n'); })    // separate by new line
            .flatten()                                      // flatten the arrays into a single array
            .map(function (n) { return parseInt(n) })       // parse all ints
            .each(function (i) {                            // check for negatives
                if (i < 0) {
                    errors.push(i);
                }
                else {
                    sum += i;
                }
            });

        if (errors.length > 0) {
            throw new Error('negatives not allowed: ' + errors.join(','));
        }

        return sum;
    };
}