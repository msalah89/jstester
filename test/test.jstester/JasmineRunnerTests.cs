﻿using System;
using System.Linq;
using System.Text;
using Moq;
using Xunit;
using System.IO;
using JsTester;

namespace Test.JsTester
{
    public class JasmineRunnerTests
    {
        public class Initialize
        {
            [Fact]
            public void should_write_js_runner_if_missing()
            {
                var runner = TestableJasmineRunner.Create();

                runner.Initialize();

                runner.FileSystem.Verify(f => f.WriteAllText(JasmineTestRunner.JASMINE_RUNNER, It.IsAny<string>()));
            }

            [Fact]
            public void should_not_write_js_runner_if_file_exists()
            {
                var runner = TestableJasmineRunner.Create();
                runner.FileSystem.Setup(f => f.Exists(JasmineTestRunner.JASMINE_RUNNER)).Returns(true);
                runner.FileSystem.Setup(f => f.WriteAllText(JasmineTestRunner.JASMINE_RUNNER, It.IsAny<string>())).Throws(
                    new InvalidOperationException("Should not have copied file."));

                runner.Initialize();
            }

            [Fact]
            public void should_write_jasmine_reporter_if_missing()
            {
                var runner = TestableJasmineRunner.Create();

                runner.Initialize();

                runner.FileSystem.Verify(f => f.WriteAllText(JasmineTestRunner.JASMINE_REPORTER, It.IsAny<string>()));
            }

            [Fact]
            public void should_not_write_jasmine_reporter_if_file_exists()
            {
                var runner = TestableJasmineRunner.Create();
                runner.FileSystem.Setup(f => f.Exists(JasmineTestRunner.JASMINE_REPORTER)).Returns(true);
                runner.FileSystem.Setup(f => f.WriteAllText(JasmineTestRunner.JASMINE_REPORTER, It.IsAny<string>())).Throws(
                    new InvalidOperationException("Should not have copied file."));

                runner.Initialize();
            }
        }

        public class RunTests
        {
            [Fact]
            public void should_throw_if_null_specs()
            {
                var runner = TestableJasmineRunner.Create();

                Assert.Throws(typeof(ArgumentNullException), () => runner.RunTests(null, 0));
            }

            [Fact]
            public void should_throw_if_spec_not_found()
            {
                var runner = TestableJasmineRunner.Create();

                Assert.Throws(typeof(FileNotFoundException), () => runner.RunTests("invalid", 0));
            }

            [Fact]
            public void should_throw_if_spec_is_missing_conditional_to_skip_execution_on_load_when_running_with_headless_browser()
            {
                var runner = TestableJasmineRunner.Create();
                runner.FileSystem.AddFile("SpecRunner.html", "<html>Contents</html>");

                Assert.Throws(typeof(InvalidOperationException), () => runner.RunTests("SpecRunner.html", 0));
            }

            [Fact]
            public void should_call_runner_with_arguments()
            {
                var runner = TestableJasmineRunner.Create();
                runner.FileSystem.AddFile("SpecRunner.html");

                runner.RunTests("SpecRunner.html", 0);

                runner.PhantomRunner.Verify(r => r.Execute(JasmineTestRunner.JASMINE_RUNNER + " SpecRunner.html", 
                    It.IsAny<int>()));
            }

            [Fact]
            public void should_throw_error_if_runner_fails()
            {
                var runner = TestableJasmineRunner.Create();
                runner.FileSystem.AddFile("SpecRunner.html");
                runner.PhantomRunner.Setup(r => r.Execute(It.IsAny<string>(), It.IsAny<int>()))
                    .Throws(new InvalidOperationException("Failed to call phantom"));

                Assert.Throws(typeof (InvalidOperationException), () => runner.RunTests("SpecRunner.html", 0));
            }

            [Fact]
            public void should_parse_output_from_phantom()
            {
                var runner = TestableJasmineRunner.Create();
                runner.FileSystem.AddFile("SpecRunner.html");
                runner.PhantomRunner.Setup(r => r.Execute(It.IsAny<string>(), It.IsAny<int>())).Returns(new string[] { 
                    "[PASS] with default delimiter -> single number should return the same number"
                });

                var result = runner.RunTests("SpecRunner.html", 0).Single();

                Assert.True(result.Passed);
                Assert.Equal("SpecRunner.html: with default delimiter -> single number should return the same number", result.Name);
            }
        }

        public class ParseOutput
        {
            // TODO: No lines from phantom
            // TODO: Error following a pass
            // TODO: Fail with no error

            [Fact]
            public void when_passing_jasmine_results()
            {
                var runner = TestableJasmineRunner.Create();
                var output = new string[] { 
                    "[PASS] Test1", 
                    "[PASS] Test2"
                };

                var results = runner.ParseOutput(output);

                Assert.Equal(2, results.Count());
                Assert.True(results.All(r => r.Passed));
                Assert.True(results.All(r => r.Name.StartsWith("Test")));
            }

            [Fact]
            public void when_failing_jasmine_result()
            {
                var runner = TestableJasmineRunner.Create();
                var output = new string[] { 
                    "[FAIL] with default delimiter : single number should return the same number",
                    "[ERROR] Expected 0 to equal 1."
                };

                var result = runner.ParseOutput(output).Single();

                Assert.False(result.Passed);
                Assert.Equal("with default delimiter : single number should return the same number", result.Name);
                Assert.Equal("Expected 0 to equal 1.", result.ErrorMessage);
                Assert.Equal(JsResult.UNKNOWN_ERROR, result.ErrorType);
            }

            [Fact]
            public void when_jasmine_result_has_multiple_errors_should_return_first_error()
            {
                var runner = TestableJasmineRunner.Create();
                var output = new string[] { 
                   "[FAIL] Test name",
                    "[ERROR] Error 1",
                    "[ERROR] Error 2",
                };

                var result = runner.ParseOutput(output).Single();

                Assert.False(result.Passed);
                Assert.Equal("Test name", result.Name);
                Assert.Equal(JsResult.UNKNOWN_ERROR, result.ErrorType);
                Assert.Equal("Error 1", result.ErrorMessage);
            }
        }

        public class TestableJasmineRunner: JasmineTestRunner
        {
            public Mock<IFileSystem> FileSystem { get; set; }
            public Mock<IProcessExecuter> PhantomRunner { get; set; }

            private TestableJasmineRunner(string workingDir, Mock<IFileSystem> fileSystem, Mock<IProcessExecuter> phantomRunner)
                : base(workingDir, fileSystem.Object, phantomRunner.Object)
            {
                FileSystem = fileSystem;
                PhantomRunner = phantomRunner;
            }

            public static TestableJasmineRunner Create(string workingDir = "")
            {
                return new TestableJasmineRunner(workingDir, new Mock<IFileSystem>(), new Mock<IProcessExecuter>());
            }
        }
    }
}
