﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;
using JsTester;

namespace Test.JsTester
{
    public class JsResultTests
    {
        [Fact]
        public void ctor_result_should_throw_if_null()
        {
            Assert.Throws(typeof(ArgumentNullException), () => JsResult.Pass(null));
            Assert.Throws(typeof(ArgumentNullException), () => JsResult.Fail((string)null));
        }

        [Fact]
        public void pass_result_default_values()
        {
            var r = JsResult.Pass("foo");

            Assert.True(r.Passed);
            Assert.Equal("foo", r.Name);
            Assert.Null(r.ErrorMessage);
            Assert.Null(r.ErrorType);
        }

        [Fact]
        public void fail_result_default_values()
        {
            var r = JsResult.Fail("foo");

            Assert.False(r.Passed);
            Assert.Equal("foo", r.Name);
            Assert.NotNull(r.ErrorMessage);
            Assert.NotNull(r.ErrorType);
        }

        [Fact]
        public void fail_result_with_exception_should_throw_if_nulls()
        {
            Assert.Throws(typeof(ArgumentNullException), () => JsResult.Fail("foo", (Exception)null));
            Assert.Throws(typeof(ArgumentNullException), () => JsResult.Fail(null, new Exception()));
        }

        [Fact]
        public void fail_result_with_exception_should_set_defaults_for_null_values()
        {
            var e = new Exception(null);
            var r = JsResult.Fail("Name", e);

            Assert.False(r.Passed);
            Assert.Equal("Name", r.Name);
            Assert.NotNull(r.ErrorMessage);
            Assert.Equal("System.Exception", r.ErrorType);
        }

        [Fact]
        public void fail_result_with_exception()
        {
            var e = new Exception("Exception Message");
            var r = JsResult.Fail("Name", e);

            Assert.False(r.Passed);
            Assert.Equal("Name", r.Name);
            Assert.Equal("System.Exception: Exception Message", r.ErrorMessage);
            Assert.Equal("System.Exception", r.ErrorType);
        }
    }
}
