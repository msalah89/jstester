using System.Collections.Generic;

namespace JsTester
{
    public interface IProcessExecuter
    {
        IEnumerable<string> Execute(string args, int timeout);
    }
}