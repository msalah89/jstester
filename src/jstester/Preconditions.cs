﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JsTester
{
    internal static class Preconditions
    {
        public static void CheckNotNull(object o, string paramName)
        {
            if (o == null)
            {
                throw new ArgumentNullException(paramName);
            }
        }

        public static void Check(bool expression, string message)
        {
            Check(expression, () => new InvalidOperationException(message));
        }

        public static void Check(bool expression, Func<Exception> exceptionFunc)
        {
            if (!expression)
            {
                throw exceptionFunc();
            }
        }
    }
}
