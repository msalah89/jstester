/**
 Jasmine Reporter that outputs test results to the browser console. 
 
 Original source from https://github.com/jcarver989/phantom-jasmine by Joshua Carver and modified for this project
*/

(function (jasmine, console) {
    if (!jasmine) {
        throw "jasmine library isn't loaded!";
    }

    var ConsoleReporter = function () {
        if (!console || !console.log) { throw "console isn't present!"; }
        this.status = this.statuses.stopped;
    };

    var proto = ConsoleReporter.prototype;
    proto.statuses = {
        stopped: "stopped",
        running: "running",
        fail: "fail",
        success: "success"
    };

    proto.reportRunnerStarting = function (runner) {
        this.status = this.statuses.running;
        this.start_time = (new Date()).getTime();
        this.executed_specs = 0;
        this.passed_specs = 0;
        this.log("Starting...");
    };

    proto.reportRunnerResults = function (runner) {
        var failed = this.executed_specs - this.passed_specs;
        var spec_str = this.executed_specs + (this.executed_specs === 1 ? " spec, " : " specs, ");
        var fail_str = failed + (failed === 1 ? " failure in " : " failures in ");
        var dur = (new Date()).getTime() - this.start_time;

        this.log("");
        this.log("Finished");
        this.log("-----------------");
        this.log(spec_str + fail_str + (dur / 1000) + "s.");

        this.status = (failed > 0) ? this.statuses.fail : this.statuses.success;

        /* Print something that signals that testing is over so that headless browsers
        like PhantomJs know when to terminate. */
        this.log("");
        this.log("ConsoleReporter finished");
    };


    proto.reportSpecStarting = function (spec) {
        this.executed_specs++;
    };

    proto.reportSpecResults = function (spec) {
        var resultText = spec.results().passed() ? "[PASS] " : "[FAIL] ";

        var parent = spec.suite.parentSuite;
        while (parent) {
            resultText += parent.description + " -> ";
            parent = parent.parentSuite;
        }
        resultText += spec.suite.description + " -> " + spec.description;

        this.log(resultText);

        if (spec.results().passed()) {
            this.passed_specs++;
            return;
        }

        var items = spec.results().getItems();
        for (var i = 0; i < items.length; i++) {
            var error = items[i].trace;
            if (error) {
                this.log("[ERROR] " + error.message);
            }
        }
    };

    proto.reportSuiteResults = function (suite) {
        if (!suite.parentSuite) { return; }
        var results = suite.results();
        var failed = results.totalCount - results.passedCount;
    };

    proto.log = function (str) {
        console.log(str);
    };

    jasmine.ConsoleReporter = ConsoleReporter;
})(jasmine, console);

var console_reporter = new jasmine.ConsoleReporter();