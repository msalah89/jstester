using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace JsTester
{
    public class PhantomjsProcessExecuter: IProcessExecuter
    {
        public const string PHANTOMJS_EXE = "phantomjs.exe";

        private readonly IFileSystem _fileSystem;
        private readonly string _workingDir;

        public PhantomjsProcessExecuter(string workingDir = null)
            : this(workingDir, new FileSystem())
        {
        }

        public PhantomjsProcessExecuter(string workingDir,IFileSystem fileSystem)
        {
            _fileSystem = fileSystem;
            _workingDir = workingDir ?? Path.GetDirectoryName(typeof (PhantomjsProcessExecuter).Assembly.Location);
        }

        public IEnumerable<string> Execute(string args, int timeout)
        {
            var phantomFilePath = Path.Combine(_workingDir, PHANTOMJS_EXE);

            var processStartInfo = new ProcessStartInfo()
            {
                FileName = _fileSystem.Exists(phantomFilePath)? phantomFilePath : PHANTOMJS_EXE,
                Arguments = args,
                ErrorDialog = false,
                UseShellExecute = false,
                CreateNoWindow = true,
                RedirectStandardOutput = true,
                RedirectStandardError = true
            };

            Console.WriteLine(String.Format("Executing '{0}' with args '{1}'", 
                processStartInfo.FileName, processStartInfo.Arguments));

            var lines = new List<string>();
            using (var process = Process.Start(processStartInfo))
            {
                process.OutputDataReceived += (sender, line) =>
                {
                    if (line.Data != null) lines.Add(line.Data);
                
                };

                process.ErrorDataReceived += (sender, error) =>
                {
                    if (error.Data != null)
                        throw new Exception("Error received from phantomjs: " + error.Data
                            + GetExecutionDetials(lines, processStartInfo));
                };

                process.BeginErrorReadLine();
                process.BeginOutputReadLine();

                if (!process.WaitForExit(timeout))
                {
                    try
                    {
                        process.Kill();
                    }
                    catch { }

                    var message = String.Format("Phantomjs process did not exit after {0} milliseconds.", timeout);
                    throw new TimeoutException(message + GetExecutionDetials(lines, processStartInfo));
                }
            }

            return lines;
        }

        private static string GetExecutionDetials(List<string> lines, ProcessStartInfo processStartInfo)
        {
            return String.Format(@"
Command: '{0} {1}'
Output: 
{2}",
                processStartInfo.FileName,
                processStartInfo.Arguments,
                String.Join(Environment.NewLine, lines));
        }
    }
}