namespace JsTester
{
    public interface IFileSystem
    {
        void WriteAllBytes(string path, byte[] bytes);
        bool Exists(string path);
        void WriteAllText(string path, string content);
        string ReadAllText(string path);
    }
}