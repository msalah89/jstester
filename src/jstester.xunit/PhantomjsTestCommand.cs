﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit.Sdk;

namespace JsTester.Xunit
{
    public class PhantomjsTestCommand: TestCommand
    {
        private readonly JsResult _result;

        public PhantomjsTestCommand(IMethodInfo methodInfo, JsResult result)
            : base(methodInfo, result.Name, 5000)
        {
            _result = result;
        }

        public override MethodResult Execute(object testClass)
        {
            if (_result.Passed)
            {
                return new PassedResult(this.MethodName, this.TypeName, _result.Name, null);
            }
            else
            {
                return new FailedResult(this.MethodName, this.TypeName, _result.Name, null,
                   _result.ErrorType, _result.ErrorMessage, String.Empty);
            }
        }
    }
}
