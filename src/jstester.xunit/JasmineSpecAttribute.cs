﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace JsTester.Xunit
{
    public class JasmineSpecAttribute: FactAttribute
    {
        private const int DEFAULT_TIMEOUT = 10000;
        
        public string SpecRunnerFile { get; set; }

        public JasmineSpecAttribute()
        {
            Timeout = DEFAULT_TIMEOUT;
        }
    }
}
