# Overview
JsTester is a library to run javascript tests from within .NET by using [phantom.js](http://phantomjs.org/) instead of a UI browser. 

# Blog Posts
* [Introduction](http://www.federicosilva.net/2012/07/run-js-tests-without-browser-from-xunit.html)

# Support
Currently, [jasmine.js](http://pivotal.github.com/jasmine) is the only javascript test framework supported. Tests can be run using any .NET framework and/or runner, but full integration support is only available for [xUnit](http://xunit.codeplex.com) .

# Requirements
Phantomjs.exe needs to be available in the path.

# Usage

## 1. Prepare the jasmine spec file
Edit the jasmine runner.html file and add a conditional to only execute the tests on load if running on a UI browser. For example:

      <html>
      <head>
        <title>Jasmine Test Runner</title>
        <!-- Additional resources -->
      </head>
      <body>
         <script type="text/javascript">
             jasmine.getEnv().addReporter(new jasmine.TrivialReporter());
             
             if (navigator.userAgent.indexOf('PhantomJS') == -1) {
                 jasmine.getEnv().execute();
             }
         </script>
      </body>
      </html>

## 2. Invoke the test from .NET
If you are using xUnit you can use the `RunWithPhantomjs` and `JasmineSpec` attributes to point to the jasmine spec file. By default, the name of the spec file is inferred from the method name, but it can also be supplied to the `JasmineSpec` attribute.

      using JsTester.Xunit;
      namespace Tests.Integration
      {
          [RunWithPhantomjs]
          public class MyJasmineTests
          {
              [JasmineSpec]
              public void TestRunner()
              {
              }
          }
      }

Now you can execute this test as part of the regular xUnit run.   
   